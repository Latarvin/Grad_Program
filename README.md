# Graduation Program

物理毕设黑历史

## 代码运行说明

1. libsvm使用可以参考https://blog.csdn.net/abcjennifer/article/details/7370177
2. 首先需要把 Grad_Program\_libsvm_setup\libsvm-3.22\matlab 添加到MATLAB运行目录下才可以使用libsvm的函数（CSDN博客步骤2A）
3. 然后在CODE目录下运行pre_process, main, ML_para三个主要函数，可以单独运行
4. 数据和存储路径在load_data.m中修改
5. 参数在load_constants.m中修改

使用愉快 :)