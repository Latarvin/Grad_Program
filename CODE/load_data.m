function [data2, id_bulk, ptn_smlrt] = load_data(i_global)
global POS_PATH RESCALED_MAT ORDER_MAT

% import data2, data_bulk, data_path, id_bulk
CT_data_path = [POS_PATH, 'CT', num2str(i_global), '\'];
load([CT_data_path, RESCALED_MAT]);
load([CT_data_path, ORDER_MAT]);
ptn_smlrt = Pattern_Similarity;

% disp('Load data completed!');
end
