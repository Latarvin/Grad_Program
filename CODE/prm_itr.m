function acc = prm_itr(prm_name, prm_range, X, Y)
acc = cell(1,length(prm_range));

parfor i = 1:length(prm_range)
    prm_str = ['-s 0 -t 2 -h 0 -e 0.01 ' prm_name ' ' num2str(prm_range(i))];
%     [~,acc_set] = cross_validation(Y,X,prm_str);  
%     acc{i} = mean(cell2mat(acc_set)); 
    
    model = svmtrain(Y,X,prm_str);
    [~,acc_tmp,~] = svmpredict(Y,X,model);
    acc{i} = acc_tmp(1);
    
    disp(['--' prm_name ' = ' num2str(prm_range(i)) ' finished---'])
end

acc = cell2mat(acc);
end
