close all, clear all, clc
% N2_max = 299;

%% load data
load_constants
X = [];
Y = [];

for i_global = 30
    [particle, idx_eff_in, ptn_smlrt] = load_data(i_global);
    load([SAVE_PATH '\CT_' num2str(i_global) '_para.mat']);
    load([SAVE_PATH '\CT_' num2str(i_global) '_D2.mat']);

    %% generate X and Y
    len = length(para);
    Y = [Y; D2_mark];  % 1 means defects
    X_tmp = cell(len,1);

    parfor i = 1:len
        X_tmp{i} = [ptn_smlrt(para(i).idx).dis, para(i).vol_fra, double(para(i).Z)];

        % T_tmp = sort(para(i).T);
        % T_tmp = T_tmp(1:20)';
        % if para(i).N2 ~= N2_max
        %     continue;
        % end
        % % define effective part, X and Y same dimension
        % G2_tmp = double(para(i).G2);
        % X_tmp = [X_tmp, sort(para(i).angle_set), T_tmp, G2_tmp];
%         X_tmp{i} = [X_tmp{i}, sort(para(i).angle_set)];

    end
    X_tmp = cell2mat(X_tmp);
    X = [X; X_tmp];
    clearvars X_tmp
end
disp('X & Y finished')

model = svmtrain(Y,X,'-s 0 -t 2 -h 0 -e 0.01 -g 0.1');
w = model.SVs' * model.sv_coef;    
b = model.rho;

%% demo
[model_set,acc_set] = cross_validation(Y,X,'-s 0 -t 2 -h 0 -e 0.01 -g 0.1');
acc_mean = mean(cell2mat(acc_set));

%% optimal para
% w1
w1_range = logspace(-1,1,5);
w1_acc = prm_itr('-w1', w1_range, X, Y);
figure
semilogx(w1_range,w1_acc)
title('w1')
grid on

% g
g_range = logspace(-1,4,10);
g_acc = prm_itr('-g', g_range, X, Y);
figure
semilogx(g_range,g_acc)
title('g')
grid on

% c
c_range = logspace(-1,4,10);
c_acc = prm_itr('-c', c_range, X, Y);
figure
semilogx(c_range,c_acc)
title('c')
grid on
