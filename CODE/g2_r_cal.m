disp('g2_r calculation start')
r = linspace(0.5,5,300);
distance_mean = (r(1:end-1) + r(2:end))/2;

%% import data
Rc = particle(:,2:4)';  % TODO Rc or pos?
Rc = Rc(:,idx_eff_in);
radi = particle(:,5)';
Dmean = 2*mean(radi);

%% get distance layers
l_range = max(Rc,[],2) - min(Rc,[],2);
dis_bdr = min(abs([ Rc - min(pos',[],2);
                    Rc - max(pos',[],2) ]));
dis_bdr = dis_bdr/Dmean;
N2_set = cell(1,len);
G2_set = cell(1,len);

%% get g2, # within each layer
for i = 1:len
    Rij = pos' - repmat(Rc(:,i),1,length(pos));
    Dij = sqrt(sum(Rij.^2,1));  % relative distance
    Dij = Dij / Dmean;
    N2_tmp = 0;  % # of layer where exists particles
    G2_tmp = zeros(1,length(distance_mean));

    for j = 1:length(distance_mean)
        if r(j) >= dis_bdr(i)
            break;
        end
        index = find(Dij >= r(j) & Dij < r(j+1));
        N2_tmp = N2_tmp + 1;
        G2_tmp(j) = numel(index);
    end
    N2_set{i} = int32(N2_tmp);
    G2_set{i} = int32(G2_tmp);
end

parfor i = 1:len
    para(i).N2 = N2_set{i};
    para(i).G2 = G2_set{i};
end

% number_density = len/prod(l_range)*Dmean^3;
% g2 = G2./N2 ./ (distance_mean.^2) ./ (r(2:end)-r(1:end-1)) / (4*pi) / number_density;

%% plot
% figure;
% G2_array = zeros(1,length(distance_mean));
% for i = 1:len
%     G2_array = G2_array + g2_set(i).G2;
% end
% plot(distance_mean,G2_array);

save([SAVE_PATH '\CT_' num2str(i_global) '_para.mat'], 'para');
disp('g2_r calculation FINISHED')

clearvars N2_set G2_set Rc radi Dmean l_range dis_bdr
% r is temp var
