%% clear the env
close all, clear all, clc
tic

%% load data and create new file
load_constants
% para_mkdir

%% calculate parameters
for i_global = 30
    disp(['CT_' num2str(i_global) ' START'])
    [particle, idx_eff_in, ptn_smlrt] = load_data(i_global);
    len = length(idx_eff_in);
    r = particle(:,5);
    pos = particle(:,2:4);
    load([SAVE_PATH '\CT_' num2str(i_global) '_para.mat']);

    voronoi_cell    % volumn fraction
    Z_cal           % coordination number
    angle_cal       % angle relationship
    T_cal           % delta from a regular tetrahedron
    g2_r_cal        % radial distribution function
    disp(['-- CT_' num2str(i_global) ' FINISHED! --'])
end

toc
