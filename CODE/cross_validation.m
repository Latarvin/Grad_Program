function [model_set,acc_set] = cross_validation(Y,X,para)
FOLDER = 3;

X = X(randperm(size(X,1)),:);
idx = 1:(floor(length(X)/10)*10);
idx_cell = mat2cell(idx,1,repmat(length(idx)/FOLDER,1,FOLDER));

model_set = cell(1,FOLDER);
acc_set = cell(1,FOLDER);

parfor i = 1:FOLDER
    Xtest = X(idx_cell{i},:);
    Ytest = Y(idx_cell{i});
    Xtrain = X;
    Xtrain(idx_cell{i},:) = [];
    Ytrain = Y;
    Ytrain(idx_cell{i}) = [];

    svm_model = svmtrain(Ytrain,Xtrain,para);
    model_set{i} = svm_model;
    [~,acc,~] = svmpredict(Ytest,Xtest,svm_model);
    acc_set{i} = acc(1);
end
  
end
