tap = [0 1 3 6 10 20 40 70 100 300 600 1000 3000 5000];
threshold_all = [1.024,1.024 1.024 1.026 1.027 1.029 1.03...
                 1.031 1.032 1.032 1.032 1.033 1.031 1.03];

Z_all = zeros(1,length(tap));
for i_global = 1:length(tap)
    tap_num = tap(i_global);
    load(['E:\dong-pro\all_data_4_new_aps\voro_cell\voro_cell' num2str(tap_num) '.mat']);

    delta = cell(1,length(idx_eff_in));
    N = zeros(1,length(idx_eff_in));
    Z = zeros(1,length(idx_eff_in));
    V_free = zeros(1,length(idx_eff_in));

    for i = 1:length(idx_eff_in)
        ii = idx_eff_in(i);
        neighbor_cur = neighbor{ii};

        dis_center = sqrt((particle(neighbor_cur,2)-particle(ii,2)).^2+...
                          (particle(neighbor_cur,3)-particle(ii,3)).^2+...
                          (particle(neighbor_cur,4)-particle(ii,4)).^2);

        % N
        N(i) = length(neighbor{ii});

        % Z
        threshold = 1.03;
        ratio_r = dis_center./(particle(ii,5)+particle(neighbor_cur,5));
        idx_contact = find(ratio_r<threshold);
        Z(i) = length(idx_contact);

        % delta
        delta_tmp = dis_center-(particle(neighbor_cur,5)+particle(ii,5));
        delta{i} = delta_tmp;

        % free volume
        ver_cur = ver{ii};
        [~,V_cell_tmp] = convhull(ver_cur);
        V_free(i) = V_cell_tmp-0.694*8*particle(ii,5)^3;

    end
    Z_all(i_global) = mean(Z);
    %save(['E:\dong-pro\all_data_4_new_aps\basic_prop\basic' num2str(tap_num) '.mat'],'N','Z','V_free','delta','idx_eff_in','particle');

end
