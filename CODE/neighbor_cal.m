disp('Neighbor calcualtion START')
tic

pos_all = particle(:,2:4);
parfor i = 1:length(idx_eff_in)
    ii = idx_eff_in(i);
    pos_loc = pos_all(ii,:);
    pos_sub = pos_all - repmat(pos_loc,length(pos_all),1);
    dist = sum(pos_sub.^2,2).^(1/2);
    dist_sort = sort(dist);
    neighbor_idx = find(dist <= dist_sort(1+NEIGHBOR_NUM) &...
        dist > 0);

    para(i).neighbor_default = int32(neighbor_idx');
end
toc

save([SAVE_PATH '\CT_' num2str(i_global) '_para.mat'], 'para');
disp('Neighbor caculation FINISHED')
