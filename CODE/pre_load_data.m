function [data2, id_bulk] = pre_load_data(i_global)
%% load data
global POS_PATH RESCALED_MAT ORDER_MAT

CT_data_path = [POS_PATH, 'CT', num2str(i_global), '\'];
load([CT_data_path, RESCALED_MAT]);
load([CT_data_path, ORDER_MAT]);
end
