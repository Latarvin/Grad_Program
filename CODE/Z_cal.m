threshold = Z_THRE;

%% pick contact ones and calculate Z
contact_idx = cell(1,len);
parfor i = 1:len
    ii = idx_eff_in(i);

    neighbor_idx = para(i).neighbor_default;
    pos_sub = pos(neighbor_idx,:)-repmat(para(i).pos,length(neighbor_idx),1);
    dis_center = sum(pos_sub.^2,2).^(1/2);

    ratio_r = dis_center ./ (para(i).r + r(neighbor_idx));
    contact_idx{i} = neighbor_idx(ratio_r < threshold);
end

parfor i = 1:len
    para(i).contact_idx = contact_idx{i};
    para(i).Z = int32(length(contact_idx{i}));
end

clearvars contact_idx

save([SAVE_PATH '\CT_' num2str(i_global) '_para.mat'], 'para');
disp('Z calculation FINISHED')

%% plot
Z_set = zeros(1,len);
for i = 1:len
    Z_set(i) = para(i).Z;
end
hist(Z_set,13);
grid on
% axis([0 12 0 7000]);
xlabel('Z')
ylabel('number')

% clearvars threshold len i_global neighbor_set i ii neighbor_idx dis_center ratio_r idx_contact
