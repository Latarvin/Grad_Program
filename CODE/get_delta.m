function delta = get_delta(pos_set,d)
% pos_set is 4*3 matrix, position set with 4 points
% delta = e_max-1
% normal value should be <0.41
p_pair = nchoosek(1:4,2);
l_set = zeros(1,length(p_pair));

for i = 1:length(p_pair)
    p = pos_set(p_pair(i,:),:);
    l_set(i) = norm(p(1,:)-p(2,:));
end

delta = max(l_set)/d-1;
end
