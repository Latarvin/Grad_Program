% caution: takes a long time
T_THRE = 0.1;  % usually 0.05
disp('T calculation start')

p_set = cell(1,len);
T_set = cell(1,len);

parfor i = 1:len
    ii = idx_eff_in(i);

    %% get T set
    p_group = nchoosek(1:NEIGHBOR_NUM,3);
    len_p = length(p_group);
    p_record = [];
    T_record = [];
    for j = 1:len_p
        p_pick = para(i).neighbor_default(p_group(j,:));
        T = edge_diff([pos(ii,:); pos(p_pick,:)]);
        if T > T_THRE
            continue;
        end
        p_record = [p_record; p_pick];
        T_record = [T_record; T];
    end
    p_set{i} = int32(p_record);
    T_set{i} = T_record;
end

parfor i = 1:len
    para(i).T_neighbor_pick = p_set{i};
    para(i).T = T_set{i};
end

clearvars p_set T_set

save([SAVE_PATH '\CT_' num2str(i_global) '_para.mat'], 'para');
disp('T calculation FINISHED')
