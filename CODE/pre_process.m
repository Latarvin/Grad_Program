%% clear the env
close all
clear
clc

%% load data and create new file
load_constants

%% calculate parameters
for i_global = IDX_RANGE
    disp(['CT_' num2str(i_global) ' pre-process START'])
    clear para
    [particle, idx_eff_in] = pre_load_data(i_global);
    struct_init
    neighbor_cal
    marking
    disp(['----CT_' num2str(i_global) ' pre-process FINISHED----'])
end
