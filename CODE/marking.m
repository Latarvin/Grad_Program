%% load data
% 35 - CT_34
load([D2_PATH D2_MAT]);
D2 = D2minavg{i_global+1};
D2(D2 > 1e3) = NaN;
D2_sort = sort(D2);

%% check dimension
% load(['G:\Github\DATA\shear_SVM_gzy\local_structures\CT'...
%     num2str(i_global) '\basic_rescaled.mat']);
% disp(length(id_bulk)==length(D2))

%% marking
thre = D2_sort(floor((1-DEFECT_RATE)*length(D2_sort)));
D2_mark = zeros(size(D2));
D2_mark(D2 > thre) = 1;

%% plot
% figure
% plot(D2,'.');
% hold on
% plot(find(D2_mark == 0),D2(D2_mark == 0),'ro');
% grid on

%% saving
save([SAVE_PATH '\CT_' num2str(i_global) '_D2.mat'],'D2','D2_mark');
disp('Marking FINISHED')
