%% constants declare
global POS_PATH RESCALED_MAT ORDER_MAT NEIGHBOR_MAT

%% constants
GIT_PATH = 'C:\Users\arvin\Git\';

POS_PATH = [GIT_PATH 'DATA\shear_SVM_gzy\local_structures\'];
RESCALED_MAT = 'basic_rescaled.mat';
ORDER_MAT = 'order_icos.mat';
D2_PATH = [GIT_PATH 'DATA\shear_SVM_gzy\nonaffine_displacements\avg_r_2.5\'];
D2_MAT = 'D2minavg_Ndc_2.5_df_2.mat';
NEIGHBOR_MAT = 'neighbor_default.mat';
NEIGHBOR_NUM = 12;
DEFECT_RATE = 0.15;

SAVE_PATH = [GIT_PATH 'DATA\svm_input\'];
IDX_RANGE = 32:35;  % TODO
Z_THRE = 1.03;  % unit: diameter
