load_constants
load([SAVE_PATH '\CT_' num2str(i_global) '_para.mat']);

% % Z
% Z_set = zeros(1,len);
% for i = 1:len
%     Z_set(i) = para(i).Z;
% end
% hist(Z_set,13);
% grid on
% % axis([0 12 0 7000]);
% xlabel('Z')
% ylabel('number')

% Z
p_set = zeros(1,len);
for i = 1:len
%     p_set(i) = para(i).vol_fra;
    p_set(i) = ptn_smlrt(i).dis;
end
hist(p_set,50);
grid on
% axis([0 1 0 2500]);
xlabel('pattern similarity')
ylabel('number')