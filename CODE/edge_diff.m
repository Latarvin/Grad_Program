function T = edge_diff(pos_set)
% pos_set is 4*3 matrix, position set with 4 points
% return Edge differences, T measure
% T = Sigma(li-lj)^2/(15*mean(l^2))
p_pair = nchoosek(1:4,2);
sum_tmp = 0;
l_set = zeros(1,length(p_pair));

for i = 1:length(p_pair)
    p = pos_set(p_pair(i,:),:);
    l_set(i) = norm(p(1,:)-p(2,:));
end

l_pair = nchoosek(1:length(l_set),2);
for i = 1:length(l_pair)
    l = l_set(l_pair(i,:));
    sum_tmp = sum_tmp + (l(1)-l(2))^2;
end
T = sum_tmp / (15*mean(l_set.^2));
end
