disp('Voronoi calcualtion start')

Rc = particle(:,2:4)';
radi = 1.0425*particle(:,5)';  % 1.0425 is correction factor
Dmean = 2*mean(radi);

[V,C] = voronoin(particle(:,2:4));
Vt = V';
parfor i = 1:length(C)
    ve = C{i};
    c = find(ve>1);
    C{i} = ve(c);
end

Vtra = cell(1,length(particle));
parfor i = 1:length(particle)
    D = C{i,1};
    for j = 1:length(D)
        D(1:3,j) = Vt(1:3,D(1,j));
    end
    Vtra{1,i} = D;
end

vcell = zeros(1,length(Vtra));
parfor i = 1:length(Vtra)
    ver_cur = Vtra{i}';
    [~,V_cell_tmp] = convhull(ver_cur);
    vcell(i) = V_cell_tmp;
end

%% set boundary condition
% bdr_eff_length = 2.5;  % remove 3 layers (2 layers of crystal)
%
% dis_bdr = min(abs([ Rc - min(Rc,[],2);
%                     Rc - max(Rc,[],2) ]));
% dis_bdr = dis_bdr/Dmean;
% idx_eff_in = find(dis_bdr >= bdr_eff_length);

%% calculate vol_fraction
vcell_eff = vcell(idx_eff_in);
vsphere_eff = 4/3*pi*radi(idx_eff_in).^3;
vol_fra_tot = sum(vsphere_eff)/sum(vcell_eff);
vol_fra = vsphere_eff ./ vcell_eff;

%% record
parfor i = 1:length(idx_eff_in)
    para(i).vol_fra = vol_fra(i);
end

save([SAVE_PATH '\CT_' num2str(i_global)...
    '_para.mat'], 'para');
disp('Voronoi calcualtion FINISHED')

clearvars V C Vt ve c i Vtra D jj Rc radi Dmean bdr_eff_length dis_bdr
