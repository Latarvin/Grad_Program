%% remove edge particles
% xyz range
xyzscope = [min(data_bulk_CT35); max(data_bulk_CT35)];
xyzscope(:,1) = []; % delete label
xyzscope(:,4) = []; % delete radius

% get xyz range of chosen particle
edge = 2.5;  % pixel
xyzscope(1,:) = xyzscope(1,:)+edge;
xyzscope(2,:) = xyzscope(2,:)-edge;

% pick feasible central particle
for i = 1:3
    core_CT35 = data_bulk_CT35(data_bulk_CT35(:,i+1) >= xyzscope(1,i)...
        & data_bulk_CT35(:,i+1) <=  xyzscope(2,i), :);
end
clear xyzscope edge

%% calculate distance
% distance matrix
Dist_CT35 = pdist2(core_CT35(:,2:4), core_CT35(:,2:4), 'euclidean');

% central particle and particles within distance r
t = 0;
r = 15.4146*2;  % r of small balls
% sg = [];
sgm = 0.02;
d = 0.5:0.1:2.5;
Gfunc_CT35 = cell(length(d), 5);
% |k = |CoreDist|expGfunc|Sum each particle|Sum total
for k = d
    t = t+1;
    Gfunc_CT35(t,1) = {strcat('k = ', num2str(k))};
    m = r * (k - sgm);
    n = r * (k + sgm);
    clear CoreDist;
    
    for i = 1:length(Dist_CT35)
        w = find(Dist_CT35(i,:) >= m & Dist_CT35(i,:) <= n);
        CoreDist(i,1:length(w)) = Dist_CT35(i,w);
        Gfunc_CT35(t,2) = {CoreDist};
    end
    
    %Gfunc
    Gexp = exp(-1*(CoreDist-k*r).^2/(sgm*r)^2);
    Gfunc_CT35(t,3) = {Gexp};
    Gfunc_particle = sum(Gexp,2);
    Gfunc_CT35(t,4) = {Gfunc_particle};
    Gfunc_CT35(t,5) = {sum(Gfunc_particle)};
    disp(['----The result for k = ', num2str(k), ' is ready!----'])
end

Gfunc_p35 = [];


%% Gfunction of each particle
for i = 1:size(d,2)
    Gfunc_p35 = [Gfunc_p35,cell2mat(Gfunc_CT35(i,4))];
end
disp('G function calculation is done!');


%% D2_c35
% load data from D2_f10, and merge with Gfunc_p35
D2_35 = cell2mat(D2minavg_f10(35,1));
D2_35 = [id_bulk_CT35, D2_35];  % fixme
D2_c35 = D2_35(ismember(D2_35(:,1), core_CT35(:,1)),:);
DG_CT35 = [D2_c35, Gfunc_p35];
% delete invalid D2 data NaN
NaNlable = find(isnan(DG_CT35(:,2)));
DG_CT35(NaNlable,:) = [];
% rearrangement number of CT35 and D2 is 22
DG35_sort = sortrows(DG_CT35,-2);

DG35_reg = DG35_sort(1:22,:);


%% mere test, needless loop
% fixme 35->36 ?
DG35_test = DG_CT35(randi(length(DG_CT35),10,1),:); % label for test
DG35_fig = [DG35_reg;DG35_test];
% plot for rearrangement and nonrearrangement particles
figure
for i = 1:size(DG35_reg,2)
    x = d;
    y = DG35_reg(:,3:end);
    plot(x,y,'--rs')
    hold on
end
for i = 1:size(DG35_test,2)
    x = d;
    y = DG35_test(:,3:end);
    plot(x,y,'--gs')
    hold on
end
disp('Test done!')
disp('==============================')

%% clear useless vars
save('Gfunc_data.mat', 'DG_CT35', 'DG35_reg', 'd', 'DG35_sort');
clear
