close all, clear all, clc
load('G:\Github\Data\shear_SVM_gzy\local_structures\CT34\order_icos.mat');

pos = zeros(3,length(Pattern_Similarity));
for i = 1:length(Pattern_Similarity)
    pos(:,i) = Pattern_Similarity(i).trans;
end

plot3(pos(1,:), pos(2,:), pos(3,:), 'b.');
grid on;
