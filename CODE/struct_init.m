len = length(idx_eff_in);
para(1,len) = struct('idx',[],'pos',[],'r',[]);

parfor i = 1:len
    ii = idx_eff_in(i);
    para(i).idx = int32(ii);
    para(i).pos = particle(ii,2:4);
    para(i).r = particle(ii,5);
end

save([SAVE_PATH '\CT_' num2str(i_global) '_para.mat'], 'para');
disp('Struct initiation FINISHED')
