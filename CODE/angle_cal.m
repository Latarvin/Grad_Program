angle_set = cell(1,len);

parfor i = 1:len
    ii = idx_eff_in(i);

    %% get distance
    % get nomorlized pos vector
    dis_tmp = zeros(NEIGHBOR_NUM,3);
    for j = 1:NEIGHBOR_NUM
        contact_cur = para(i).neighbor_default(j);
        r = pos(contact_cur,:) - pos(ii,:);
        dis_tmp(j,:) = r / norm(r);  % normalize pos vector r
    end

    x = dis_tmp(:,1)';
    y = dis_tmp(:,2)';
    z = dis_tmp(:,3)';

    bond_diff = repmat(x,NEIGHBOR_NUM,1).*repmat(x',1,NEIGHBOR_NUM)+...
                repmat(y,NEIGHBOR_NUM,1).*repmat(y',1,NEIGHBOR_NUM)+...
                repmat(z,NEIGHBOR_NUM,1).*repmat(z',1,NEIGHBOR_NUM);
    bond_diff_angle = acos(bond_diff);

    angle_set{i} = setdiff(unique(bond_diff_angle),diag(bond_diff_angle))';
end

parfor i = 1:len
    para(i).angle_set = angle_set{i};
end

clearvars angle_set

%% save the file
save([SAVE_PATH '\CT_' num2str(i_global) '_para.mat'], 'para');
disp('Angle calculation FINISHED')
