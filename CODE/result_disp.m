T_num = zeros(1,len);
N2_num = zeros(1,len);

for i = 1:len
    T_tmp = para(i).T;
    T_tmp = T_tmp(T_tmp > 0.05);
    T_num(i) = length(T_tmp);
    N2_num(i) = para(i).N2;
end

plot(T_num,'.')
figure
plot(N2_num,'.')

min(T_num)
min(N2_num)
