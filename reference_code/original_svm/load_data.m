%% constants
data_path = 'G:\Github\data\shear_SVM_gzy\';
CT_idx = 35;
rescaled_file = 'basic_rescaled.mat';
NAD_idx = 10;
range_avg = 2.5;  % unit r


%% load data
CT_data_path = [data_path, 'local_structures\CT',...
    num2str(CT_idx-1), '\'];  % TODO, -1?
load([CT_data_path, rescaled_file]);
% order_file = 'order_icos.mat';
% load([data_path, order_file]);
% clear rescaled_file order_file
clear CT_idx CT_data_path rescaled_file
% import data2, data_bulk, data_path, id_bulk

% nonaffine displacement (NAD) data
NAD_data_path = [data_path, 'nonaffine_displacements\',...
    'avg_r_', num2str(range_avg), '\'];
NAD_file = ['D2minavg_Ndc_', num2str(range_avg), '_df_', num2str(NAD_idx), '.mat'];
load([NAD_data_path, NAD_file]);
clear NAD_idx NAD_data_path NAD_file

% change var name
D2minavg_f10 = D2minavg;
data_bulk_CT35 = data_bulk;
id_bulk_CT35 = id_bulk;
clear D2minavg data_bulk id_bulk
% import D2minavg_f10, D2mineps, D2minsig

disp('Load data completed!')
