%% load vars
load('Gfunc_data.mat');


%% initialize test_label and train_label
label = -1*ones(size(DG_CT35,1),1);
label(ismember(DG_CT35(:,1),DG35_reg(:,1)),:) = 1;
% set is in the front with large ismember

% num of train sample tsize = 22, ts tr has been exchanged
tsize = round(0.5*size(label,1));
Tslabel = label(1:tsize,:);
Trlabel = label(tsize+1:end,:);
Tsdata = DG_CT35(1:tsize,3:end);
Trdata = DG_CT35(tsize+1:end,3:end);

% 2017-5-8 revise
T1label = ones(22,1);
T1data = DG35_sort(1:22,3:end);

% 2017-5-8 test
T0label = -1*ones(100,1);
T0data = DG35_sort(23:122,3:end);


%% Machine Learning
model = svmtrain_new(Trlabel,Trdata,'-s 2 -t 1 -c 0.9 -g 0.1');  % TODO -h 0 ?
[~,acctrain,~] = svmpredict(Trlabel,Trdata,model);
[~,acctest,~] = svmpredict(Tslabel,Tsdata,model);
[~,acctest1,~] = svmpredict(T1label,T1data,model);
[ptest0,acctest0,~] = svmpredict(T0label,T0data,model);

% loop test
p = 1;
for m = 0.9:0.01:1.1
    q = 1;
    for n = 1:10:100
        model = svmtrain_new(Trlabel,Trdata,strcat('-s 2 -t 1 -c ',...
            num2str(m), ' -g ', num2str(n)));
        [~,acctrain,~] = svmpredict(Trlabel,Trdata,model);
        [~,acctest,~] = svmpredict(Tslabel,Tsdata,model);
        [~,acctest1,~] = svmpredict(T1label,T1data,model);
        [ptest0,acctest0,~] = svmpredict(T0label,T0data,model);

        set{p,q} = [acctrain(1),acctest1(1),acctest0(1)];
        q = q+1;
    end

    disp(['====The result for m = ', num2str(m), ' is ready!===='])
    p = p+1;
end


% save test result
result(1,:) = [acctrain(1),acctest(1),acctest1(1)];

% collection = CT35
% sgm = 0.02| DG_CT35|Trlabel|Tslabel|Trdata|Trdata|result
% collection = {};
for i = 6
    collection(i,1) = {'CT35 sgm = 0.02'};
    collection(i,2) = {DG_CT35};
    collection(i,3) = {Trlabel};
    collection(i,4) = {Tslabel};
    collection(i,5) = {Trdata};
    collection(i,6) = {Trdata};
    collection(i,7) = {result};
end


%% initialize more model with +1 value
% combine the +1 value from 30~35 and half the -1 in the 35, then test
% latter half of the -1 in 35 and all the +1 in 30~35
pTrlabel = ones(132,1);
v = 1;
for i = 35  % TODO 30:35
    eval(['pTrdata(v:v+21,:) = DG', num2str(i), '_sort(1:22,3:end);']);
    v = v+22;
end
label = -1*ones(size(DG35_sort,1)-22+size(DG35_sort,1)-22,1);
data = [DG35_sort(23:end,3:end);DG35_sort(23:end,3:end)];

tsize = round(0.8*size(label,1));
Trlabel = [label(1:tsize,:);pTrlabel];
Trdata = [data(1:tsize,:);pTrdata];
nTrlabel = label(1:tsize,:);
nTrdata = data(1:tsize,:);
Tslabel = label(tsize+1:end,:);
Tsdata = data(tsize+1:end,:);

model = svmtrain_new(Trlabel, Trdata, '-s 2 -t 2 -c 1 -g 0.396');
% [ptrain,acctrain] = svmpredict(Trlabel,Trdata,model);
% [ptest,acctest] = svmpredict(Tslabel,Tsdata,model);
[ptest1,acctest1,~] = svmpredict(pTrlabel,pTrdata,model);
[ptest2,acctest2,~] = svmpredict(nTrlabel,nTrdata,model);

% loop test 0.396 with the LARGEST sum in the 49th data
% Accuracy  =  47.6563% (3355/7040) (classification)
% Accuracy  =  77.8373% (5377/6908) (classification)
% Accuracy  =  71.2121% (94/132) (classification)


%% loop test for para choose
p = 0;
for m = 0.9:0.01:1.1
    p = p+1;
    q = 0;
    for n = 0.2:0.05:0.4
        q = q+1;
        str = ['-s 2 -t 2 -c 1 -g ',num2str(n)];
        model = svmtrain_new(Trlabel,Trdata,str);
        [~,acctrain,~] = svmpredict(Trlabel,Trdata,model);
        [~,acctest,~] = svmpredict(Tslabel,Tsdata,model);
        [ptest1,acctest1,~] = svmpredict(pTrlabel,pTrdata,model);
        [ptest2,acctest2,~] = svmpredict(nTrlabel,nTrdata,model);

        set(q,:) = [n,acctrain(1),acctest(1),...
            acctest1(1),acctest2(1)];

    end
    disp(['====The result for m = ', num2str(m), ' is ready!===='])

end

% sum
for i = 1:51
    sumset(i,1) = sum(set(1,i),2);
    setplus(i,:) = cell2mat(set(1,i));
end

setplus = set(:,4)+set(:,5);
find(setplus == max(setplus))


%% lib collapse?, randi version

pTrdata = [];
nTrdata = [];
DG_sort3039 = [];

for i = 30:2:39
    eval(['DG_sort3039 = [DG_sort3039; DG',num2str(i),'_sort];']);
end

tsize = size(DG_sort3039,1);  % total size
DG_sort3039 = sortrows(DG_sort3039,-2);
scope = find(DG_sort3039(:,2)>= 300);
psize = size(scope,1);  % +1 size
pTrdata = DG_sort3039(scope,3:end);
pTrlabel = ones(size(pTrdata,1),1);

num = randi([psize+1,tsize],1,5000);  % set the num of -1 in the model
nTrdata = DG_sort3039(num,3:end);
nTrlabel = -1*ones(size(nTrdata,1),1);

% initialize test
scope = find(DG35_sort(:,2)>= 300);
ppsize = size(scope,1);
pdata = DG35_sort(1:ppsize,3:end);
plabel = ones(size(pdata,1),1);

ttsize = size(DG35_sort,1);
num = randi([ppsize+1,ttsize],1,500);  % set the num of -1 in the test
ndata = DG35_sort(num,3:end);
nlabel = -1*ones(size(ndata,1),1);

Trlabel = [pTrlabel;nTrlabel];
Trdata = [pTrdata;nTrdata];
Tslabel = [plabel;nlabel];
Tsdata = [pdata;ndata];

tic
model = svmtrain_new(Trlabel,Trdata,'-s 0 -t 2 -w1 3 -w-1 0.06 -g 0.055');
[ptrain,acctrain,~] = svmpredict(Trlabel,Trdata,model);
[ptest,acctest,~] = svmpredict(Tslabel,Tsdata,model);
[ptestp,acctestp,~] = svmpredict(pTrlabel,pTrdata,model);
[ptestn,acctestn,~] = svmpredict(nTrlabel,nTrdata,model);
[ptestpp,acctestpp,~] = svmpredict(plabel,pdata,model);
[ptestnn,acctestnn,~] = svmpredict(nlabel,ndata,model);
toc


%% test loop
p = 0;
for i = 31:2:39
    p = p+1;
    DGtest = eval(['DG',num2str(i),'_sort;']);
    scope = find(DGtest(:,2)>= 300);
    ppsize = size(scope,1);
    pdata = DGtest(1:ppsize,3:end);
    plabel = ones(size(pdata,1),1);

    ttsize = size(DGtest,1);
    num = randi([ppsize+1,ttsize],1,500);  % set the num of -1 in the test
    ndata = DGtest(num,3:end);
    nlabel = -1*ones(size(ndata,1),1);

    Trlabel = [pTrlabel;nTrlabel];
    Trdata = [pTrdata;nTrdata];
    Tslabel = [plabel;nlabel];
    Tsdata = [pdata;ndata];

    tic
    model = svmtrain_new(Trlabel,Trdata,'-s 0 -t 2 -w1 3 -w-1 0.06 -g 0.055');

    disp(['====This is the result for CT',num2str(i),'!===='])
    [ptrain,acctrain,~] = svmpredict(Trlabel,Trdata,model);
    [ptest,acctest,~] = svmpredict(Tslabel,Tsdata,model);
    [ptestp,acctestp,~] = svmpredict(pTrlabel,pTrdata,model);
    [ptestn,acctestn,~] = svmpredict(nTrlabel,nTrdata,model);
    [ptestpp,acctestpp,~] = svmpredict(plabel,pdata,model);
    [ptestnn,acctestnn,~] = svmpredict(nlabel,ndata,model);
    toc

    collectionCT3139(p,:) = [i,acctrain(1),acctest(1),acctestp(1),...
        acctestn(1),acctestpp(1),acctestnn(1)];

end

% test loop for para choose
q = 0;
for g = 0.03:0.002:0.06
    for w1 = 1:0.5:10
        for w2 = 0.01:0.02:0.5
            q = q+1;
            str = ['-s 0 -t 2 -w1 ',num2str(w1),' -w-1 ',num2str(w2)];
            model = svmtrain_new(Trlabel,Trdata,str);
            disp(['====This is the result for w1 = ',num2str(w1),...
                ' w2 = ',num2str(w2),'!===='])
            [ptrain,acctrain,~] = svmpredict(Trlabel,Trdata,model);
            [ptest,acctest,~] = svmpredict(Tslabel,Tsdata,model);
            [ptestp,acctestp,~] = svmpredict(pTrlabel,pTrdata,model);
            [ptestn,acctestn,~] = svmpredict(nTrlabel,nTrdata,model);
            [ptestpp,acctestpp,~] = svmpredict(plabel,pdata,model);
            [ptestnn,acctestnn,~] = svmpredict(nlabel,ndata,model);

            collection35(q,:) = [g,w1,w2,...
                acctrain(1),acctest(1),acctestp(1),...
                acctestn(1),acctestpp(1),acctestnn(1)];
        end
    end
end

% model = svmtrain_new(Trlabel,Trdata,'-s 0 -t 2 -w1 100 -w-1 0.05 -g 0.02 -v 5');


%% super test loop for para choose
% result: g = 0.055, w1 = 3, w2 = 0.06
pTrdata = [];
nTrdata = [];
DG_sort3039 = [];
GfuncMAX = [];

for i = 30:2:39
    eval(['DG_sort3039 = [DG_sort3039; DG',num2str(i),'_sort];']);
end

tsize = size(DG_sort3039,1);  % total size
DG_sort3039 = sortrows(DG_sort3039,-2);
scope = find(DG_sort3039(:,2)>= 300);
psize = size(scope,1);  % +1 size
pTrdata = DG_sort3039(scope,3:end);
pTrlabel = ones(size(pTrdata,1),1);

num = randi([psize+1,tsize],1,5000);  % set the num of -1 in the model
nTrdata = DG_sort3039(num,3:end);
nTrlabel = -1*ones(size(nTrdata,1),1);

q = 0;
for i = 31:2:39
    DGtest = eval(['DG',num2str(i),'_sort;']);
    scope = find(DGtest(:,2) >= 300);
    ppsize = size(scope,1);
    pdata = DGtest(1:ppsize,3:end);
    plabel = ones(size(pdata,1),1);

    ttsize = size(DGtest,1);
    num = randi([ppsize+1,ttsize],1,500);  % set the num of -1 in the test
    ndata = DGtest(num,3:end);
    nlabel = -1*ones(size(ndata,1),1);

    Trlabel = [pTrlabel;nTrlabel];
    Trdata = [pTrdata;nTrdata];
    Tslabel = [plabel;nlabel];
    Tsdata = [pdata;ndata];

    for g = 0.01:0.005:0.06
        for w1 = 1:1:20
            for w2 = 0.01:0.05:0.5
                q = q+1;
                str = ['-s 0 -t 2 -w1 ',num2str(w1),' -w-1 ',num2str(w2)];
                model = svmtrain_new(Trlabel,Trdata,str);
                disp(['====This is the result for CT',num2str(i),...
                    ' g = ',num2str(g),' w1 = ',num2str(w1),...
                    ' w2 = ',num2str(w2),'!===='])
                [ptrain,acctrain,~] = svmpredict(Trlabel,Trdata,model);
                [ptest,acctest,~] = svmpredict(Tslabel,Tsdata,model);
                [ptestp,acctestp,~] = svmpredict(pTrlabel,pTrdata,model);
                [ptestn,acctestn,~] = svmpredict(nTrlabel,nTrdata,model);
                [ptestpp,acctestpp,~] = svmpredict(plabel,pdata,model);
                [ptestnn,acctestnn,~] = svmpredict(nlabel,ndata,model);

                GfuncMAX(q,:) = [i,g,w1,w2,...
                    acctrain(1),acctest(1),acctestp(1),...
                    acctestn(1),acctestpp(1),acctestnn(1)];
            end
        end
    end
end
