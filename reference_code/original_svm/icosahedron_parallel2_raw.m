DS_sort3039 = [];
DS_sort3039 = [];
pTrdata = [];
pTrlabel = [];
nTrdata = [];
nTrlabel = [];


for i = 30:2:39
    eval(['DS_sort3039 = [DS_sort3039; DS_sort',num2str(i),'];']);
end
DS_sort3039 = sortrows(DS_sort3039,-2);
scope = find(DS_sort3039(:,2) >= 300);
pTrdata = DS_sort3039(scope,3:end);
pTrlabel = ones(size(pTrdata,1),1);

num = randi([size(scope,1)+1,size(DS_sort3039,1)],1,5000);
nTrdata = DS_sort3039(num,3:end);
nTrlabel = -1*ones(size(nTrdata,1),1);

% q = 0;
irng = 31:2:39;
grng = [0.000005:0.0000025:0.00001, 0.00002:0.000025:0.0001,...
    0.0002:0.00025:0.001, 0.002:0.0025:0.01];
w1rng = 1:1:20;
w2rng = 0.05:0.05:0.5;
TSidesMAX = cell(length(irng),1);
for ii = 1%:length(irng);
    tic
    i = irng(ii);
    DStest = eval(['DS_sort',num2str(i)]);   
    scope = find(DStest(:,2) >= 300);
    ppsize = size(scope,1);
    pdata = DStest(scope,3:end);
    plabel = ones(ppsize,1);
    num = randi([ppsize+1,size(DStest,1)],1,500);
    ndata = DStest(num,3:end);
    nlabel = -1*ones(size(ndata,1),1);
    Trlabel = [pTrlabel;nTrlabel];
    Trdata = [pTrdata;nTrdata];
    Tslabel = [plabel;nlabel];
    Tsdata = [pdata;ndata];
    
    TSidesMAXtmp = cell(length(grng),length(w1rng),length(w2rng));
    
    for ig = 1:length(grng)
        for iw1 = 1:length(w1rng)
            parfor iw2 = 1:length(w2rng)
%                 q = q+1;
                
                g = grng(ig);
                w1 = w1rng(iw1);
                w2 = w2rng(iw2);
                str = ['-s 0 -t 2 -w1 ',num2str(w1),' -w-1 ',num2str(w2),...
                    ' -g ',num2str(g)];
                model = svmtrain(Trlabel,Trdata,str);
                disp(['----This is the result for CT',num2str(i),...
                    ' g = ',num2str(g),' w1 = ',num2str(w1),...
                    ' w2 = ',num2str(w2),'!----'])
                [ptrain,acctrain] = svmpredict(Trlabel,Trdata,model);
                [ptest,acctest] = svmpredict(Tslabel,Tsdata,model);
                [ptestp,acctestp] = svmpredict(pTrlabel,pTrdata,model);
                [ptestn,acctestn] = svmpredict(nTrlabel,nTrdata,model);
                [ptestpp,acctestpp] = svmpredict(plabel,pdata,model);
                [ptestnn,acctestnn] = svmpredict(nlabel,ndata,model);

%                 TSidesMAX(q,:) = [i,g,w1,w2,...
%                     acctrain(1,1),acctest(1,1),acctestp(1,1),...
%                     acctestn(1,1),acctestpp(1,1),acctestnn(1,1)];
                TSidesMAXtmp{ig,iw1,iw2} = [i,g,w1,w2,...
                    acctrain(1,1),acctest(1,1),acctestp(1,1),...
                    acctestn(1,1),acctestpp(1,1),acctestnn(1,1)];
                
            end
        end
    end
    TSidesMAXtmp = cell2mat(TSidesMAXtmp(:));
    TSidesMAX{ii} = TSidesMAXtmp;
    toc
end
TSidesMAX = cell2mat(TSidesMAX);