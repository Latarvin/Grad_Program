function [idx_order,Pattern_Similarity] = order_icosa_(Rc)
% function of calculation of icosahedra order parameter
% by xiachj 20170526


%% icosahedron coordinates
m_icos=sqrt(50-10*sqrt(5))/10;
n_icos=sqrt(50+10*sqrt(5))/10;
A=[m_icos m_icos -m_icos -m_icos 0 0 0 0 n_icos n_icos -n_icos -n_icos;...
    0 0 0 0 n_icos n_icos -n_icos -n_icos m_icos -m_icos m_icos -m_icos;...
    n_icos -n_icos n_icos -n_icos m_icos -m_icos m_icos -m_icos 0 0 0 0];


%% sort particle order of (perfect) icosahedron
z_axis=A(:,1);% select z axis
z_proj=sum(repmat(z_axis,1,size(A,2)).*A,1);% z coordinates in z axis
idx_seq=[1 find(z_proj>0&z_proj~=1) find(z_proj<0&z_proj~=-1) find(z_proj==min(z_proj))];% sort first according to z axis
x_axis=A(:,idx_seq(2))-z_axis*dot(z_axis,A(:,idx_seq(2)));% select x axis
x_axis=x_axis/norm(x_axis);
y_axis=cross(z_axis,x_axis);% get y axis
x_proj=sum(repmat(x_axis,1,size(A,2)).*A,1);% x coordinate
y_proj=sum(repmat(y_axis,1,size(A,2)).*A,1);% y coordinate
[th,~,~]=cart2pol(x_proj,y_proj,z_proj);% get angular coordinate
th=th+2*pi*(th<0);

% [~,Itmp]=sort(th(idx_seq(2:6)));% sort according to angular coordinate for those with z>0
% idx_tmp=idx_seq(2:6);
% idx_seq(2:6)=idx_tmp(Itmp);
% [~,Itmp]=sort(th(idx_seq(7:11)));
% idx_tmp=idx_seq(7:11);
% idx_seq(7:11)=idx_tmp(Itmp);

[~,Itmp]=sort(th(idx_seq(2:11)));
idx_tmp=idx_seq(2:11);
idx_seq(2:11)=idx_tmp(Itmp);

A=A(:,idx_seq);
A=[zeros(3,1) A];


%%
idx_order=cell(size(Rc,2),1);
Pattern_Similarity=struct('dis',[],'rot',[],'trans',[],'scale',[],'seq',[]);

parfor ii=1:size(Rc,2)
    tic
    
    %% get index of neighboring 12 particles
    dis_tmp=sqrt(sum((Rc-repmat(Rc(:,ii),1,size(Rc,2))).^2,1));
    dis_sort=sort(dis_tmp,'ascend');
    ntmp=find(dis_tmp<=dis_sort(13));
    idx_order{ii}=ntmp;% index of center and neighbor 12 particles
    ntmp=ntmp(ntmp~=ii);
    B0=Rc(:,ntmp);
    
    %% sort particle index
    % loop for 12 start particle and 10 different loops
    d_min=10^5;
    Pattern_Similarity_tmp=struct('dis',[],'rot',[],'trans',[],'scale',[],'seq',[]);
    
    for jj=1:12 % each particle may be the top (start) particle 
        B=B0-repmat(Rc(:,ii),1,size(B0,2));
        z_axis=B(:,jj);%z_axis=B(:,1);
        z_axis=z_axis/norm(z_axis);
        z_proj=sum(repmat(z_axis,1,size(B,2)).*B,1);% z coordinates in z axis
        
        for kk=1:10% 5% after defining the start and end particle, loop for other 10 particles according to angular coordiantes
            [~,idx_seq]=sort(z_proj,'descend');
            B=B0-repmat(Rc(:,ii),1,size(B0,2));
            
            idx_seq_th=kk+1;% 2 3 4 5 6 % idx_seq_th=2;
            x_axis=B(:,idx_seq(idx_seq_th))-z_axis*dot(z_axis,B(:,idx_seq(idx_seq_th)));% select x axis
            x_axis=x_axis/norm(x_axis);
            y_axis=cross(z_axis,x_axis);
            x_proj=sum(repmat(x_axis,1,size(B,2)).*B,1);% x coordinate
            y_proj=sum(repmat(y_axis,1,size(B,2)).*B,1);
            
            [th,~,~]=cart2pol(x_proj,y_proj,z_proj);
            th=th+2*pi*(th<0);
%             [~,Itmp]=sort(th(idx_seq(2:6)));
%             idx_tmp=idx_seq(2:6);
%             idx_seq(2:6)=idx_tmp(Itmp);
%             [~,Itmp]=sort(th(idx_seq(7:11)));
%             idx_tmp=idx_seq(7:11);
%             idx_seq(7:11)=idx_tmp(Itmp);
            
            [~,Itmp]=sort(th(idx_seq(2:11)));
            idx_tmp=idx_seq(2:11);
            idx_seq(2:11)=idx_tmp(Itmp);
            
            B=B(:,idx_seq);
            B=B+repmat(Rc(:,ii),1,size(B,2));
            B=[Rc(:,ii) B];
            
            [d_tmp,R_svd,c_tmp,t_svd]=cluster_distance_(B,A);
            
            if d_tmp<d_min
                d_min=d_tmp;
                Pattern_Similarity_tmp.dis=d_tmp;
                Pattern_Similarity_tmp.rot=R_svd;
                Pattern_Similarity_tmp.trans=t_svd;
                Pattern_Similarity_tmp.scale=c_tmp;
                Pattern_Similarity_tmp.seq=idx_seq;
            end
        end
    end
    
    %% interchange ecah pair of particles to find better fit
    idx_inter=nchoosek(1:12,2);
    change_or_not=1;
    while change_or_not~=0% loop for all possible interchanging pair
        idx_seq0=Pattern_Similarity_tmp.seq;% old sequence
        change_or_not=0;
        
        for jj=1:size(idx_inter,1)
            idx_seq2=idx_seq0;
            idx_seq2([idx_inter(jj,1) idx_inter(jj,2)])=idx_seq2([idx_inter(jj,2) idx_inter(jj,1)]);% interchange two number
            
            B=B0-repmat(Rc(:,ii),1,size(B0,2));
            B=B(:,idx_seq2);
            B=B+repmat(Rc(:,ii),1,size(B,2));
            B=[Rc(:,ii) B];
            
            [d_tmp,R_svd,c_tmp,t_svd]=cluster_distance_(B,A);
            
            if d_tmp<d_min% if this sequence has better fitting
                d_min=d_tmp;% get the new distance
                Pattern_Similarity_tmp.dis=d_tmp;
                Pattern_Similarity_tmp.rot=R_svd;
                Pattern_Similarity_tmp.trans=t_svd;
                Pattern_Similarity_tmp.scale=c_tmp;
                Pattern_Similarity_tmp.seq=idx_seq2;% store the new better sequence
                change_or_not=1;
            end
        end
    end
    
    Pattern_Similarity(ii)=Pattern_Similarity_tmp;

    %%
toc
end % 7 min 10 parfor

disp('done.')
end

