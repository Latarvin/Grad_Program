%% by xiachj 20150824
% comment by xcj 170929: rotate, move, rescale A to fit B

%%
function [Dis,Rot,Sca,Trans]=cluster_distance_(A,B)
% calculate cluster distance and corresponding rotational and translational matrix, and scaling ratio
% input coordinates A and B in m rows and n columns: m = dimension n= particle number

if sum(size(A)==size(B))~=2
    error('input cluster size must be same')
end

miu_A=mean(A,2);
sigma_A=mean(sum((A-repmat(miu_A,1,size(A,2))).^2,1));
miu_B=mean(B,2);
sigma_B=mean(sum((B-repmat(miu_B,1,size(B,2))).^2,1));
%Sigma_AB=(A-repmat(miu_A,1,size(A,2)))*(B-repmat(miu_B,1,size(B,2)))'/size(A,2); % wrong
Sigma_AB=(B-repmat(miu_B,1,size(B,2)))*(A-repmat(miu_A,1,size(A,2)))'/size(A,2);% right xia 161006

[U_svd,D_svd,V_svd]=svd(Sigma_AB);
S_svd=eye(3);
S_svd(3,3)=2*(det(D_svd)>=0)-1;
Dis=sigma_B-trace(D_svd*S_svd)^2/sigma_A;

if rank(Sigma_AB)==2
    S_svd(3,3)=det(U_svd)*det(V_svd);
end

Rot=U_svd*S_svd*V_svd';
Sca=trace(D_svd*S_svd)/sigma_A;
Trans=miu_B-Sca*Rot*miu_A;
end

